package com.test.digitalberry.digitalberry_test.model.models;



public class Movement {
    private String id;
    private String type;
    private Float somme;
    private Currency currency;

    public Movement(String id, String type, Float somme, Currency currency) {
        this.id = id;
        this.type = type;
        this.somme = somme;
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getSomme() {
        return somme;
    }

    public void setSomme(Float somme) {
        this.somme = somme;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
