package com.test.digitalberry.digitalberry_test.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.test.digitalberry.digitalberry_test.R;
import com.test.digitalberry.digitalberry_test.model.models.Movement;
import com.test.digitalberry.digitalberry_test.utils.MyFormatter;

import java.util.ArrayList;

public class MovementAdapter extends ArrayAdapter<Movement> {

    private ArrayList<Movement> mMovementsList;

    public MovementAdapter(Context context, int resource, ArrayList<Movement> objects) {
        super(context, resource, objects);
        mMovementsList=objects;
    }

    @Override
    public int getCount() {
        if(mMovementsList != null )
            return mMovementsList.size();
        return 0;
    }

    @Override
    public Movement getItem(int position) {
        return mMovementsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, @NonNull ViewGroup parent) {
        return getView(position, cnvtView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.history_list_item,null);
        }
        Movement movement = mMovementsList.get(position);
        if (movement != null) {
            TextView prod_txt = (TextView) v.findViewById(R.id.type_mouvement);
            prod_txt.setText(movement.getType());
            TextView ca_txt = (TextView) v.findViewById(R.id.montant);
            ca_txt.setText(MyFormatter.formatterEUR(movement.getSomme()));
        }
        return v;
    }


}