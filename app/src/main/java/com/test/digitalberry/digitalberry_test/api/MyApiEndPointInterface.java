package com.test.digitalberry.digitalberry_test.api;

import com.google.gson.JsonObject;
import com.test.digitalberry.digitalberry_test.model.models.Currency;
import com.test.digitalberry.digitalberry_test.model.models.LoginResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface MyApiEndPointInterface {
    @GET("currency/?code={code}")
    Call<Currency> getCurrency(@Path("code") String code);
    @POST("login/")
    Call<LoginResponse> getToken(@Body JsonObject loginPwd);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://tech-tests.digitalberry.fr/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}

