package com.test.digitalberry.digitalberry_test;

import android.app.Application;

import com.test.digitalberry.digitalberry_test.model.manager.WalletManager;
import com.test.digitalberry.digitalberry_test.model.models.Currency;
import com.test.digitalberry.digitalberry_test.model.models.Movement;
import com.test.digitalberry.digitalberry_test.model.models.Wallet;


public class WalletApplication extends Application {
    public Wallet mWallet;
    @Override
    public void onCreate() {
        super.onCreate();
        mWallet= new Wallet();
        mWallet.setTotal(56.10f);
        WalletManager walletManager = new WalletManager();
        Currency euro = new Currency("01","EURO","€");
        Movement movement1 = new Movement("01","credit",12.6f,euro);
        Movement movement2 = new Movement("02","debit",10.2f,euro);
        walletManager.addMovement(mWallet,movement1);
        walletManager.addMovement(mWallet,movement2);
    }
    public Wallet getmWallet() {
        return mWallet;
    }
    public void setmWallet(Wallet wallet) {
         mWallet=wallet ;
    }



}
