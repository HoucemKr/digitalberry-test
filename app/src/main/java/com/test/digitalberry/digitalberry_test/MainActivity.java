package com.test.digitalberry.digitalberry_test;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.test.digitalberry.digitalberry_test.api.MyApiEndPointInterface;
import com.test.digitalberry.digitalberry_test.model.manager.WalletManager;
import com.test.digitalberry.digitalberry_test.model.models.Currency;
import com.test.digitalberry.digitalberry_test.model.models.LoginResponse;
import com.test.digitalberry.digitalberry_test.model.models.Movement;
import com.test.digitalberry.digitalberry_test.model.models.Wallet;
import com.test.digitalberry.digitalberry_test.utils.MyFormatter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.test.digitalberry.digitalberry_test.api.MyApiEndPointInterface.retrofit;


public class MainActivity extends AppCompatActivity {

    Wallet mWallet=null;
    Float inputvalue;
    WalletManager walletManager;
    WalletApplication mWalletClass;
    Currency euro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mWallet= new Wallet();
//        mWallet.setTotal(56.10f);
        mWalletClass= (WalletApplication)getApplicationContext();
        mWallet=mWalletClass.getmWallet();
        walletManager = new WalletManager();
        updateTotal();
        euro = new Currency("EUR","EURO","€");
//        getCurrency ();
        getToken();

    }

    public void addDebit(View view){
        LayoutInflater inflater = LayoutInflater.from(this);
        final View textenter = inflater.inflate(R.layout.dialog_add, null);
        final EditText userinput = (EditText) textenter.findViewById(R.id.item_added);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(textenter)
                .setTitle("Ajouter Débit");
        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                inputvalue = Float.valueOf( userinput.getText().toString());
                Movement movementD = new Movement("02","debit",inputvalue,euro);
                walletManager.addMovement(mWallet,movementD);
                mWalletClass.setmWallet(mWallet);
                updateTotal();
            }
        })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.create();
        builder.show();
    }
    public void addCredit(View view){
        LayoutInflater inflater = LayoutInflater.from(this);
        final View textenter = inflater.inflate(R.layout.dialog_add, null);
        final EditText userinput = (EditText) textenter.findViewById(R.id.item_added);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(textenter)
                .setTitle("Ajouter Crédit");
        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                inputvalue = Float.valueOf( userinput.getText().toString());
                Movement movementC = new Movement("02","credit",inputvalue,euro);
                walletManager.addMovement(mWallet,movementC);
                mWalletClass.setmWallet(mWallet);
                updateTotal();
            }
        })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        builder.show();
    }
    public void updateTotal(){
        TextView tv_total = (TextView)findViewById(R.id.value);
        tv_total.setText(MyFormatter.formatterEUR(mWallet.getTotal()));
    }
    public void openHistoric(View view){
        startActivity(new Intent(MainActivity.this,HistoryActivity.class));
    }
    private void retrofitGetCurrency(){
        MyApiEndPointInterface myApiEndPointInterface  = retrofit.create(MyApiEndPointInterface.class);
        Call<Currency> call = myApiEndPointInterface.getCurrency("USD") ;
        call.enqueue(new Callback<Currency>() {
            @Override
            public void onResponse(Call<Currency> call, Response<Currency> response) {
                int statusCode = response.code();
                Currency currency = response.body();
                Log.d("Retrofit Call",currency.getCode()+" "+ currency.getName()+" "+ currency.getSymbol()+" "+statusCode);
            }

            @Override
            public void onFailure(Call<Currency> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }
    private void getToken(){
        JsonObject loginPwd = new JsonObject();
        loginPwd.addProperty("username","admin");
        loginPwd.addProperty("password","password123");
        MyApiEndPointInterface myApiEndPointInterface  = retrofit.create(MyApiEndPointInterface.class);
        Call<LoginResponse> call  = myApiEndPointInterface.getToken(loginPwd) ;
        Log.d("RetroGetLogin: ", loginPwd.toString());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                int statusCode = response.code();
                LoginResponse loginResponse = response.body();
                Log.d("Call Token Status ", Integer.toString(statusCode) );
                if (statusCode == 200)
                Log.d("Retrofit Call Token, ", loginResponse.getToken() );
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }
}
