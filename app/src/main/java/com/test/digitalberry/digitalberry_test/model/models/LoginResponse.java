package com.test.digitalberry.digitalberry_test.model.models;

public class LoginResponse {
    private String token;
    private String id;
    private String email;

    public LoginResponse() {
    }

    public LoginResponse(String token, String id, String email) {
        this.token = token;
        this.id = id;
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
