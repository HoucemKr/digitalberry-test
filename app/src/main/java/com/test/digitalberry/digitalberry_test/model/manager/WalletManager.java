package com.test.digitalberry.digitalberry_test.model.manager;

import com.test.digitalberry.digitalberry_test.model.models.Movement;
import com.test.digitalberry.digitalberry_test.model.models.Wallet;


public final class WalletManager {

    public void addMovement(Wallet wallet, Movement movement){
        if(movement.getType().equals("debit")) {
            wallet.setTotal(wallet.getTotal() + movement.getSomme());
            wallet.movements.add(movement);
        }
        if(movement.getType().equals("credit")){
            wallet.setTotal(wallet.getTotal()-movement.getSomme());
            wallet.movements.add(movement);
        }
        if(movement.getType().equals("change")){
            wallet.setTotal(wallet.getTotal()-movement.getSomme());
            wallet.movements.add(movement);
        }

    }
}
