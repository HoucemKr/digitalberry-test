package com.test.digitalberry.digitalberry_test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.test.digitalberry.digitalberry_test.adapters.MovementAdapter;
import com.test.digitalberry.digitalberry_test.model.models.Wallet;

public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ListView listView = (ListView)findViewById(R.id.listview);
        WalletApplication mWalletClass= (WalletApplication)getApplicationContext();
        Wallet Wallet=mWalletClass.getmWallet();
        MovementAdapter adapter = new MovementAdapter(getBaseContext(),R.layout.history_list_item,Wallet.getMovements());
        listView.setAdapter(adapter);
    }
}
