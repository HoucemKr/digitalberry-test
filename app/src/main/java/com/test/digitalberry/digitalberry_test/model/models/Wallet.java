package com.test.digitalberry.digitalberry_test.model.models;

import java.util.ArrayList;



public class Wallet {
    private String id;
    private User user;
    public ArrayList<Movement> movements;
    private Float total;

    public Wallet() {
        this.movements = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Movement> getMovements() {
        return movements;
    }

    public void setMovements(ArrayList<Movement> movements) {
        this.movements = movements;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }
}
