package com.test.digitalberry.digitalberry_test.utils;


import java.util.Locale;

public final class MyFormatter {

    public static String formatterEUR(Float mValue){
            return String.format(Locale.FRANCE,"%.2f",mValue)+ " €";
    }
}
